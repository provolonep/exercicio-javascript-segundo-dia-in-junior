
// Exercício 1
gods.forEach(god => console.log(god.name))
 

// Exercício 2
let mider = gods.filter(god => god.roles.includes("Mid"))
console.log(mider)


// Exercício 3
let sor = gods.sort((a,b) => {
  if (a.pantheon > b.pantheon){
    return 1
  } else if (a.pantheon < b.pantheon) {
    return -1
  }
  return 0
})
console.log(sor)


// Exercício 4
gods2 =[]
gods.forEach(god => gods2.push(god.name+ " ("+ god.class+")"))
console.log(gods2)

// Exercício 5 
let gods3 = (gods.filter(god =>  god.class === ("Hunter")
&& god.pantheon === ("Greek")
&& god.roles.includes("Mid")))
console.log(gods3)

